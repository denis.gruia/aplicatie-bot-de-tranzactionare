# Aplicatie bot de tranzactionare

## Desriere
Aplicația oferă utilizatorilor următoarele funcționalități:

- Autentificarea prin intermediul cheilor API
- Citirea prețului si a balanței fiecărei cryptomonede
- Calcularea automata a valorii tuturor cryptomonedelor
- Afișarea graficului liniar pentru in raport cu dolarul a unei cryptomonede
- Crearea si configurarea botului de tranzacționare
- Pornirea si oprirea unui bot de tranzacționare după numele acestuia

## Descarcare proiect
- deschidem calea directorului unde dorim sa descarcam codul susrsa intr-un terminal
- instrumentul de versionare Git trebuie sa fie instalat si configurat
- apelam coamanda: ``git clone https://gitlab.upt.ro/denis.gruia/aplicatie-bot-de-tranzactionare.git``

## Librarii necesare
Pentru ca aplicația sa poata fi generata din codul sursa, următoarele librarii si software-uri sunt necesare:
- Android Studio (mediu de dezvoltare integrat)
- JDK 8 (Kit de dezvoltare Java)
- Un telefon cu sistem de operare Android sau un emulator.

Conexiunea la internet este necesara pentru aplicatie sa realizeze comunicarea cu serverul Binance cat si pentru Android Studio sa poată descărca celelalte dependințe adăugate in fișierul de configurare Gradle. Următoarele dependințe sunt adăugate sub forma de fișier JAR(Java Archive):
- Binance API Client
- MPCharts
- Google GSON

## pasii de compilare
- In Android Studio se deschide proiectul
- Se verifica daca Java este configurat corect in Android Studio pentru ca acesta sa poata compila codul
- Din panou, se da build la proiect

## pasii de instalare si lansare
- Din Android Studio se selecteaza un dispozitiv (virtual sau fizic)
- Din panou se apasa pe butonul de run, apoi aplicatia se instaleaza si se ruleaza
- Aplicatia ramane instala pe dispotiv si se poate rula oricand